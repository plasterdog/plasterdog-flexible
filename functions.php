<?php
/**
 * plasterdogflexible functions and definitions
 *
 * @package plasterdogflexible
 */
require_once( __DIR__ . '/inc/customizer-styles.php');
require_once( __DIR__ . '/inc/simplify-profiles.php');
require_once( __DIR__ . '/inc/widget-styling.php');
require_once( __DIR__ . '/inc/backend-experience.php');
require_once( __DIR__ . '/inc/global-fields.php');
//require_once( __DIR__ . '/inc/advanced-custom-fields.php');
//require_once( __DIR__ . '/inc/required-plugins.php');
/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'plasterdogflexible_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function plasterdogflexible_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on plasterdogflexible, use a find and replace
	 * to change 'plasterdogflexible' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'plasterdogflexible', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'plasterdogflexible' ),
		'footer' => __( 'Footer Menu', 'plasterdogflexible' ),
	) );
	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'plasterdogflexible_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );


	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'comment-list',
		'search-form',
		'comment-form',
		'gallery',
	) );
}
endif; // plasterdogflexible_setup
add_action( 'after_setup_theme', 'plasterdogflexible_setup' );


//DETERMINES WHICH CUSTOMIZER PANELS ARE HIDDEN
//https://wordpress.stackexchange.com/questions/250349/how-to-remove-menus-section-from-wordpress-theme-customizer

function mytheme_customize_register( $wp_customize ) {
  //All our sections, settings, and controls will be added here

  //$wp_customize->remove_section( 'title_tagline');
  //$wp_customize->remove_section( 'background_image');
  $wp_customize->remove_panel( 'nav_menus');
  $wp_customize->remove_panel( 'widgets');
  $wp_customize->remove_section( 'static_front_page');
}
add_action( 'customize_register', 'mytheme_customize_register',50 );

/**
 * Register widgetized area and update sidebar with default widgets.
 */
function plasterdogflexible_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Archive & Post Sidebar', 'plasterdogflexible' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>'
	) );

        register_sidebar( array(
        'name'          => __( 'Regular Page Sidebar', 'plasterdogflexible' ),
        'id'            => 'sidebar-3',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>'
    ) );


 }
add_action( 'widgets_init', 'plasterdogflexible_widgets_init' );

/**
 * Enqueue scripts and styles.
 */

function plasterdogflexible_scripts() {

wp_enqueue_style( 'reset-styles', get_template_directory_uri() . '/css/reset-styles.css',false,'1.1','all');

	wp_enqueue_style( 'plasterdogflexible-style', get_stylesheet_uri() );

	wp_enqueue_script( 'plasterdogflexible-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'plasterdogflexible-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'plasterdogflexible_scripts' );

/**
 * Implement the Custom Header feature. - JMC ACTIVATED
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
//JMC => additional image size
function pw_add_image_sizes() {
    add_image_size( 'large-thumb', 300, 300, true );

}
add_action( 'init', 'pw_add_image_sizes' );
 
function pw_show_image_sizes($sizes) {
    $sizes['large-thumb'] = __( 'Custom Thumb', 'plasterdogflexible' );

 
    return $sizes;
}
add_filter('image_size_names_choose', 'pw_show_image_sizes');

 /* JMC --* Enable support for Post Thumbnails	 */
	add_theme_support( 'post-thumbnails' );

/* JMC because it is required */
	add_theme_support( 'title-tag' );

//JMC remove anchor link from more tag

function remove_more_anchor($link) {
     $offset = strpos($link, '#more-');
     if ($offset) {
          $end = strpos($link, '"',$offset);
     }
     if ($end) {
          $link = substr_replace($link, '', $offset, $end-$offset);
     }
     return $link;
}
add_filter('the_content_more_link', 'remove_more_anchor');

//JMC-https://carriedils.com/add-editor-style/ <= will apply the stylesheet to the editor view

add_action( 'init', 'cd_add_editor_styles' );
/**
 * Apply theme's stylesheet to the visual editor.
 *
 * @uses add_editor_style() Links a stylesheet to visual editor
 * @uses get_stylesheet_uri() Returns URI of theme stylesheet
 */
function cd_add_editor_styles() {
 add_editor_style( get_stylesheet_uri() );
}
//JMC- https://www.advancedcustomfields.com/resources/how-to-hide-acf-menu-from-clients/
//add_filter('acf/settings/show_admin', '__return_false');

// REGISTERING THE BLOCK 

add_action('acf/init', 'my_acf_init');
function my_acf_init() {
	
	// check function exists
	if( function_exists('acf_register_block') ) {
		
		// #1 register a bio block
		acf_register_block(array(
			'name'		=> 'bio',
			'title'		=> __('Bio'),
			'description'	=> __('A custom bio block.'),
			'render_template'	=> 'inc/blocks/bio.php',
			'category'		=> 'formatting',
			'icon'		=> 'businessman',
			// icon is a dashicon https://developer.wordpress.org/resource/dashicons/
			'keywords'	=> array( 'bio', 'plasterdog', 'custom' ),
			'enqueue_style'	=> get_stylesheet_directory_uri() . '/inc/blocks/biostyle.css',
		));

		// #2 register a feature quote block
		acf_register_block(array(
			'name'		=> 'featurequote',
			'title'		=> __('Feature Quote'),
			'description'	=> __('A custom feature quote block.'),
			'render_template'	=> 'inc/blocks/quote.php',
			'category'		=> 'formatting',
			'icon'		=> 'format-quote',
			// icon is a dashicon https://developer.wordpress.org/resource/dashicons/
			'keywords'	=> array( 'feature', 'quote', 'plasterdog', 'custom' ),
			'enqueue_style'	=> get_stylesheet_directory_uri() . '/inc/blocks/quotestyle.css',
		));	

		// #3 register a hero image block
		acf_register_block(array(
			'name'		=> 'heroimage',
			'title'		=> __('Hero Image'),
			'description'	=> __('A custom hero image block.'),
			'render_template'	=> 'inc/blocks/hero.php',
			'category'		=> 'formatting',
			'icon'		=> 'nametag',
			// icon is a dashicon https://developer.wordpress.org/resource/dashicons/
			'keywords'	=> array( 'hero', 'plasterdog', 'custom' ),
			'enqueue_style'	=> get_stylesheet_directory_uri() . '/inc/blocks/herostyle.css',
		));		

		// #4 register a triptych block
		acf_register_block(array(
			'name'		=> 'triptych',
			'title'		=> __('Triptych'),
			'description'	=> __('A custom triptych block.'),
			'render_template'	=> 'inc/blocks/triptych.php',
			'category'		=> 'formatting',
			'icon'		=> 'images-alt',
			// icon is a dashicon https://developer.wordpress.org/resource/dashicons/
			'keywords'	=> array( 'triptych', 'plasterdog', 'custom' ),
			'enqueue_style'	=> get_stylesheet_directory_uri() . '/inc/blocks/triptychstyle.css',
		));	

		// #5 register a call to action block
		acf_register_block(array(
			'name'		=> 'cta',
			'title'		=> __('Call to Action'),
			'description'	=> __('A custom call to action block.'),
			'render_template'	=> 'inc/blocks/cta.php',
			'category'		=> 'formatting',
			'icon'		=> 'megaphone',
			// icon is a dashicon https://developer.wordpress.org/resource/dashicons/
			'keywords'	=> array( 'cta','call to action', 'plasterdog', 'custom' ),
			'enqueue_style'	=> get_stylesheet_directory_uri() . '/inc/blocks/ctastyle.css',
		));

		// #6 register aa accordion block
		acf_register_block(array(
			'name'		=> 'accordion',
			'title'		=> __('Accordion'),
			'description'	=> __('A custom call to action block.'),
			'render_template'	=> 'inc/blocks/accordion.php',
			'category'		=> 'formatting',
			'icon'		=> 'welcome-learn-more',
			// icon is a dashicon https://developer.wordpress.org/resource/dashicons/
			'keywords'	=> array( 'cta','call to action', 'plasterdog', 'custom' ),
			'enqueue_style'	=> get_stylesheet_directory_uri() . '/inc/blocks/accordion.css',
			'enqueue_script' => get_template_directory_uri() . '/inc/blocks/accordion.js',
		));		


	}
}

// RENDERING THE BLOCK

function my_acf_block_render_callback( $block ) {
	
	// convert name ("acf/bio") into path friendly slug ("bio")
	$slug = str_replace('acf/', '', $block['name']);
	
	// include a template part from within the "template-parts/block" folder for the block
	if( file_exists( get_theme_file_path("/inc/blocks/bio-{$slug}.php") ) ) {
		include( get_theme_file_path("/inc/blocks/bio-{$slug}.php") );
	}

	// include a template part from within the "template-parts/block" folder for the block
	if( file_exists( get_theme_file_path("/inc/blocks/quote-{$slug}.php") ) ) {
		include( get_theme_file_path("/inc/blocks/quote-{$slug}.php") );
	}

	// include a template part from within the "template-parts/block" folder for the block
	if( file_exists( get_theme_file_path("/inc/blocks/hero-{$slug}.php") ) ) {
		include( get_theme_file_path("/inc/blocks/hero-{$slug}.php") );
	}

	// include a template part from within the "template-parts/block" folder for the block
	if( file_exists( get_theme_file_path("/inc/blocks/triptych-{$slug}.php") ) ) {
		include( get_theme_file_path("/inc/blocks/triptych-{$slug}.php") );
	}

	// include a template part from within the "template-parts/block" folder for the block
	if( file_exists( get_theme_file_path("/inc/blocks/cta-{$slug}.php") ) ) {
		include( get_theme_file_path("/inc/blocks/cta-{$slug}.php") );
	}

	// include a template part from within the "template-parts/block" folder for the block
	if( file_exists( get_theme_file_path("/inc/blocks/accordion-{$slug}.php") ) ) {
		include( get_theme_file_path("/inc/blocks/accordion-{$slug}.php") );
	}



}


//https://rudrastyh.com/gutenberg/remove-default-blocks.html
add_filter( 'allowed_block_types', 'misha_allowed_block_types' );
 
function misha_allowed_block_types( $allowed_blocks ) {
 
	return array(
		'core/image',
		'core/paragraph',
		'core/heading',
		'core/list',
		'core/gallery',
		'core/audio',
		'core/code',
		'core/freeform',
		'core/html',
		'core/columns',
		'core/button',
		'core/separator',
		'core/shortcode',
		'core/search',
		'core/table',
		'core/embed',
		'core/spacer',
		'core/group',
		'acf/accordion',
		'acf/bio',
		'acf/cta',
		'acf/triptych',
		'acf/heroimage',
		'acf/featurequote',		
	);
 
}