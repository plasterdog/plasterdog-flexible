<?php
// JMC Simplify Profiles

/* JMC --* https://snippets.khromov.se/remove-admin-color-scheme-from-user-admin-dashboard-in-wordpress-3-8/    */
remove_action('admin_color_scheme_picker', 'admin_color_scheme_picker');

// JMC - Remove fields from Admin profile page https://pluginsreviews.com/remove-personal-options-section/
function wordpress_remove_user_personal_options($personal_options) {
 $personal_options = preg_replace('#<tr class="user-rich-editing-wrap(.*?)</tr>#s', '', $personal_options, 1); // Remove the "Visual Editor" field
 $personal_options = preg_replace('#<tr class="user-comment-shortcuts-wrap(.*?)</tr>#s', '', $personal_options, 1); // Remove the "Keyboard Shortcuts"
 return $personal_options;
}
function wordpress_profile_subject_start() {
 ob_start('wordpress_remove_user_personal_options');
}
function wordpress_profile_subject_end() {
 ob_end_flush();
}
// Hooks.
add_action('admin_head', 'wordpress_profile_subject_start');
add_action('admin_footer', 'wordpress_profile_subject_end');

/* JMC --* https://davidwalsh.name/add-profile-fields    */

function modify_contact_methods($profile_fields) {


    // Remove old fields
    unset($profile_fields['twitter']);
    unset($profile_fields['googleplus']);
    unset($profile_fields['facebook']);

    return $profile_fields;
}
add_filter('user_contactmethods', 'modify_contact_methods');
