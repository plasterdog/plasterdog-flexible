<?php
//JMC => https://code.tutsplus.com/tutorials/a-guide-to-the-wordpress-theme-customizer-adding-a-new-setting--wp-33180
//CUSTOMIZER COLOR ADDITIONS

//SETTING UP THE NEW COLOR CONTROL: LINKS
function pdog1_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_link_color',
        array(
            'default'     => '#00649c',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'link_color',
            array(
                'label'      => __( 'Link Color', 'plasterdogflexible' ),
                'section'    => 'colors',
                'settings'   => 'pdog_link_color'
            )
        )
    );
}
add_action( 'customize_register', 'pdog1_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog1_customizer_css() {
    ?>
    <style type="text/css">
       a { color: <?php echo get_theme_mod( 'pdog_link_color' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog1_customizer_css' );
//SETTING UP THE NEW COLOR CONTROL: LINKS:HOVER
function pdog2_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_link_hover',
        array(
            'default'     => '#001f30',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'link_hover',
            array(
                'label'      => __( 'Link Hover', 'plasterdogflexible' ),
                'section'    => 'colors',
                'settings'   => 'pdog_link_hover'
            )
        )
    );
}
add_action( 'customize_register', 'pdog2_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog2_customizer_css() {
    ?>
    <style type="text/css">
       a:hover { color: <?php echo get_theme_mod( 'pdog_link_hover' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog2_customizer_css' );
 //SETTING UP THE NEW COLOR CONTROL: NAVIGATION LINKS
function pdog1a_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_nav_link_color',
        array(
            'default'     => '#000000',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'nav_link_color',
            array(
                'label'      => __( 'Nav Link Color', 'plasterdogflexible' ),
                'section'    => 'colors',
                'settings'   => 'pdog_nav_link_color'
            )
        )
    );
}
add_action( 'customize_register', 'pdog1a_register_theme_customizer' );

//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog1a_customizer_css() {
    ?>
    <style type="text/css">
     .main-navigation  a  { color: <?php echo get_theme_mod( 'pdog_nav_link_color' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog1a_customizer_css' );

//SETTING UP THE NEW COLOR CONTROL: NAV LINKS:HOVER
function pdog2a_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_nav_link_hover',
        array(
            'default'     => '#cccccc',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'nav_link_hover',
            array(
                'label'      => __( 'Nav Link Hover', 'plasterdogflexible' ),
                'section'    => 'colors',
                'settings'   => 'pdog_nav_link_hover'
            )
        )
    );
}
add_action( 'customize_register', 'pdog2a_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog2a_customizer_css() {
    ?>
    <style type="text/css">
       .main-navigation li a:hover,  .main-navigation a:hover, .main-navigation li:hover a{ color: <?php echo get_theme_mod( 'pdog_nav_link_hover' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog2a_customizer_css' );


//SETTING UP THE NEW COLOR CONTROL: SOCIAL MEDIA LINKS
function pdog3_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_social_color',
        array(
            'default'     => '#000000',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'social_color',
            array(
                'label'      => __( 'Social Media Link Color', 'plasterdogflexible' ),
                'section'    => 'colors',
                'settings'   => 'pdog_social_color'
            )
        )
    );
}
add_action( 'customize_register', 'pdog3_register_theme_customizer' );


//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog3_customizer_css() {
    ?>
    <style type="text/css">
      #right-head .social a,  #right-head .social, #right-head .social .icon-text{ color: <?php echo get_theme_mod( 'pdog_social_color' ); ?>; }

    </style>
    <?php
}
add_action( 'wp_head', 'pdog3_customizer_css' );

//SETTING UP THE NEW COLOR CONTROL: SOCIAL MEDIA LINKS:HOVER
function pdog4_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_social_hover',
        array(
            'default'     => '#001f30',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'social_hover',
            array(
                'label'      => __( 'Social Media Link Hover', 'plasterdogflexible' ),
                'section'    => 'colors',
                'settings'   => 'pdog_social_hover'
            )
        )
    );
}
add_action( 'customize_register', 'pdog4_register_theme_customizer' );

//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog4_customizer_css() {
    ?>
    <style type="text/css">
       #right-head .social a:hover,  #right-head .social:hover, #right-head .social .icon-text:hover{ color: <?php echo get_theme_mod( 'pdog_social_hover' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog4_customizer_css' );

//SETTING UP THE NEW COLOR CONTROL: HEADINGS COLOR
function pdog5_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_headings_color',
        array(
            'default'     => '#322215',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'headings_color',
            array(
                'label'      => __( 'Headings Color', 'plasterdogflexible' ),
                'section'    => 'colors',
                'settings'   => 'pdog_headings_color'
            )
        )
    );
}
add_action( 'customize_register', 'pdog5_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog5_customizer_css() {
    ?>
    <style type="text/css">
      h1, h2, h3, h4, h5, h6, h1.page-title, h1.entry-title,h1.page-title a, h1.entry-title a, h1.responsive-page-title, h1.responsive-page-title a, h1.answer-heading, .single-question h1.page-title, .inner-focus-item h3,.inner-focus-item h3 a { color: <?php echo get_theme_mod( 'pdog_headings_color' ); ?>; }
      button, input[type="button"], input[type="reset"], input[type="submit"]{ background-color: <?php echo get_theme_mod( 'pdog_headings_color' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog5_customizer_css' );
//SETTING UP THE NEW COLOR CONTROL: BODY TEXT
function pdog6_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_body_text',
        array(
            'default'     => '#000000',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'body_text',
            array(
                'label'      => __( 'Body Text Color', 'plasterdogflexible' ),
                'section'    => 'colors',
                'settings'   => 'pdog_body_text'
            )
        )
    );
}
add_action( 'customize_register', 'pdog6_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog6_customizer_css() {
    ?>
    <style type="text/css">
       body { color: <?php echo get_theme_mod( 'pdog_body_text' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog6_customizer_css' );


//SETTING UP THE NEW COLOR CONTROL: HEADER BACKGROUND COLOR
function pdog8_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_header_background',
        array(
            'default'     => '#cccccc',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'header_background',
            array(
                'label'      => __( 'Header Area Background', 'plasterdogflexible' ),
                'section'    => 'colors',
                'settings'   => 'pdog_header_background'
            )
        )
    );
}
add_action( 'customize_register', 'pdog8_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog8_customizer_css() {
    ?>
    <style type="text/css">
      #full-top, .focus-group { background: <?php echo get_theme_mod( 'pdog_header_background' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog8_customizer_css' );

//SETTING UP THE NEW COLOR CONTROL: FOOTER BACKGROUND COLOR
function pdog9_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_foot_background',
        array(
            'default'     => '#cccccc',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'foot_background',
            array(
                'label'      => __( 'Footer Area Background', 'plasterdogflexible' ),
                'section'    => 'colors',
                'settings'   => 'pdog_foot_background'
            )
        )
    );
}
add_action( 'customize_register', 'pdog9_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog9_customizer_css() {
    ?>
    <style type="text/css">
      #colophon.site-footer { background: <?php echo get_theme_mod( 'pdog_foot_background' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog9_customizer_css' );

//SETTING UP THE NEW COLOR CONTROL: FOOTER TEXT COLOR
function pdog10_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_foot_text',
        array(
            'default'     => '#000000',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'foot_text',
            array(
                'label'      => __( 'Footer Text Color', 'plasterdogflexible' ),
                'section'    => 'colors',
                'settings'   => 'pdog_foot_text'
            )
        )
    );
}
add_action( 'customize_register', 'pdog10_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog10_customizer_css() {
    ?>
    <style type="text/css">
      #foot-constraint, #colophon.site-footer a,  #colophon.site-footer a:hover {color: <?php echo get_theme_mod( 'pdog_foot_text' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog10_customizer_css' );

//SETTING UP THE NEW COLOR CONTROL: BORDER COLOR
function pdog11_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_border_color',
        array(
            'default'     => '#000000',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'border_color',
            array(
                'label'      => __( 'Border Color', 'plasterdogflexible' ),
                'section'    => 'colors',
                'settings'   => 'pdog_border_color'
            )
        )
    );
}
add_action( 'customize_register', 'pdog11_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog11_customizer_css() {
    ?>
    <style type="text/css">
    #full-top {border-bottom: solid 1px <?php echo get_theme_mod( 'pdog_border_color' ); ?>;}
    #colophon.site-footer {border-top: solid 1px <?php echo get_theme_mod( 'pdog_border_color' ); ?>;}
    .feature-icon img {border: solid 1px <?php echo get_theme_mod( 'pdog_border_color' ); ?>;}

    @media screen and (max-width:725px){ 
.toggled .menu-main-menu-container{border-left: solid 1px <?php echo get_theme_mod( 'pdog_border_color' ); ?>;border-right: solid 1px <?php echo get_theme_mod( 'pdog_border_color' ); ?>;border-bottom: solid 1px <?php echo get_theme_mod( 'pdog_border_color' ); ?>;}
}
   </style>
    <?php
}
add_action( 'wp_head', 'pdog11_customizer_css' );

