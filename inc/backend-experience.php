<?php
//JMC - backend-experience

// JMC- unregister widgets selectively
 function unregister_default_widgets() {
     unregister_widget('WP_Widget_Pages');
     unregister_widget('WP_Widget_Calendar');
     unregister_widget('WP_Widget_Categories');
     unregister_widget('WP_Widget_Meta');
     unregister_widget('WP_Widget_Archives');
     unregister_widget('WP_Widget_Recent_Posts');
     unregister_widget('WP_Widget_Recent_Comments');
     unregister_widget('WP_Widget_RSS');
     unregister_widget('WP_Widget_Tag_Cloud');
 }
 add_action('widgets_init', 'unregister_default_widgets', 11);

//* -JMC-Replace WordPress login logo with your own
add_action('login_head', 'b3m_custom_login_logo');
function b3m_custom_login_logo() {
echo '<style type="text/css">
h1 a { background-image:url('.get_stylesheet_directory_uri().'/images/login.png) !important; background-size: 250px 150px !important;height: 150px !important; width: 250px !important; margin-bottom: 0 !important; padding-bottom: 0 !important; }
.login form { margin-top: 10px !important; }
</style>';
}

// JMC changing the logo link https://www.wpbeginner.com/wp-tutorials/how-to-change-the-login-logo-url-in-wordpress/
add_filter( 'login_headerurl', 'custom_loginlogo_url' );
function custom_loginlogo_url($url) {
    return 'https://plasterdog.com';
}

// JMC- custom footer message
function modify_footer_admin () {
  echo 'Themed and configured by plasterdog web design. ';
  echo 'CMS Powered by WordPress';
}
add_filter('admin_footer_text', 'modify_footer_admin');

// JMC Remove WordPress Widgets from Dashboard Area
function remove_wp_dashboard_widgets(){

    remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');  // Incoming Links
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side');  // Quick Press
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');  // Recent Drafts
    remove_meta_box('dashboard_primary', 'dashboard', 'side');   // WordPress blog (News)
    
}
add_action('wp_dashboard_setup', 'remove_wp_dashboard_widgets');

//Remove  WordPress Welcome Panel
remove_action('welcome_panel', 'wp_welcome_panel');

// JMC - change the standard wordpress greeting
add_action( 'admin_bar_menu', 'wp_admin_bar_my_custom_account_menu', 11 );

function wp_admin_bar_my_custom_account_menu( $wp_admin_bar ) {
$user_id = get_current_user_id();
$current_user = wp_get_current_user();
$profile_url = get_edit_profile_url( $user_id );

if ( 0 != $user_id ) {
/* Add the "My Account" menu */
$avatar = get_avatar( $user_id, 28 );
$howdy = sprintf( __('Start editing, %1$s','plasterdogflexible'), $current_user->display_name );
$class = empty( $avatar ) ? '' : 'with-avatar';

$wp_admin_bar->add_menu( array(
'id' => 'my-account',
'parent' => 'top-secondary',
'title' => $howdy . $avatar,
'href' => $profile_url,
'meta' => array(
'class' => $class,
),
) );

}
}

// JMC - COMBINING BOTH CUSTOM WIDGETS INTO A SINGLE WIDGET
add_action('wp_dashboard_setup', 'my_dashboard_widgets');

function my_dashboard_widgets() {
     global $wp_meta_boxes;
     unset(
          $wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins'],
          $wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary'],
         $wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']
     );

add_meta_box( 'dashboard_custom_feed', 'SPECIAL INSTRUCTIONS FOR THE PLASTERDOG FLEXIBLE THEME', 'dashboard_custom_feed_output', 'dashboard', 'side', 'high' );
}
function dashboard_custom_feed_output() {
     echo '<div class="rss-widget">';
     echo '<p style="margin:0 0 .5em 0;"><span style="font-weight: 900;">RECOMMENDED PLUGINS:</span> There are 6 custom blocks related to this theme which require Advanced Custom Fields Pro</p>
     <hr/>
     <p style="margin:0 0 .5em 0;"><span style="font-weight: 900;">CUSTOM BLOCKS:</span></p>
        <ul style="margin:0 0 0 2em;">
       <li style="margin:0 0 0 1em; list-style-type: circle;"><span style="font-weight: 900;">Bio</span><br/>
       controls:portrait image; name; title; description; contact & social media links (in accordion section; orientation) </li>
       <li style="margin:0 0 0 1em; list-style-type: circle;"><span style="font-weight: 900;">Feature Quote</span><br/>
       controls: font style; font-size; background color; quote content; quote attribution; text color; border color and width</li>
       <li style="margin:0 0 0 1em; list-style-type: circle;"><span style="font-weight: 900;">Hero Image</span><br/>
       controls: full width image; call to action text; call to action link target</li>
       <li style="margin:0 0 0 1em; list-style-type: circle;"><span style="font-weight: 900;">Triptych</span><br/>
       controls: each item found in accordion, background color; title (will be linked if target is set); content; link; link target</li>
       <li style="margin:0 0 0 1em; list-style-type: circle;"><span style="font-weight: 900;">Call to Action</span><br/>
       (link is contained in a circle icon, text is wrapped around circle) controls:icon side; icon background color; icon text; link label; link target URL; wrapping text </li>
       <li style="margin:0 0 0 1em; list-style-type: circle;"><span style="font-weight: 900;">Accordion</span><br/>
       controls: faq background color; faq text color; accordion teaser; accordion content</li>
        </ul>
        <br/>
        <p style="margin:0 0 .5em 0;">All of these blocks can be found in the "formatting" tab - or by searching for their name - or the terms "custom" - or the term "plasterdog"</p>
        <br/><hr/><br/>
        <p style="margin:0 0 .5em 0;"><span style="font-weight: 900;">ALLOWED BLOCKS:</span></p>
        <p>There are many experimental blocks out there which are not particularly useful, this theme will hide those demonstration blocks from the selector.<br/>
        here is the list of the 18 allowed core blocks:</p>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th width="50%" align="left" valign="top" scope="col">
        <ul style="margin:0 0 0 2em;">
        <li style="margin:0 0 0 1em; list-style-type: circle;">audio</li>
        <li style="margin:0 0 0 1em; list-style-type: circle;">button</li>
        <li style="margin:0 0 0 1em; list-style-type: circle;">code</li>
        <li style="margin:0 0 0 1em; list-style-type: circle;">columns</li>
        <li style="margin:0 0 0 1em; list-style-type: circle;">embed</li>
        <li style="margin:0 0 0 1em; list-style-type: circle;">freeform</li>
        <li style="margin:0 0 0 1em; list-style-type: circle;">gallery</li>
        <li style="margin:0 0 0 1em; list-style-type: circle;">group</li>
        <li style="margin:0 0 0 1em; list-style-type: circle;">heading</li>
        </ul>
    </th>
    <th width="50%" align="left" valign="top" scope="col">
        <ul style="margin:0 0 0 2em;">
        <li style="margin:0 0 0 1em; list-style-type: circle;">html</li>
        <li style="margin:0 0 0 1em; list-style-type: circle;">image</li>
        <li style="margin:0 0 0 1em; list-style-type: circle;">list</li>
        <li style="margin:0 0 0 1em; list-style-type: circle;">paragraph</li>
        <li style="margin:0 0 0 1em; list-style-type: circle;">search</li>
        <li style="margin:0 0 0 1em; list-style-type: circle;">separator</li>
        <li style="margin:0 0 0 1em; list-style-type: circle;">shortcode</li>
        <li style="margin:0 0 0 1em; list-style-type: circle;">spacer</li>
        <li style="margin:0 0 0 1em; list-style-type: circle;">table</li>
        </ul>
    </th>
  </tr>
</table>          
<br/><hr/><br/>

        <p style="margin:0 0 .5em 0;"><span style="font-weight: 900;">CUSTOMIZER CONTROLS:</span> This theme has a significantly expanded customizer which includes controls over most color selections</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th width="50%" align="left" valign="top" scope="col">        
        <ul style="margin:0 0 0 2em;">
            <li style="margin:0 0 0 1em; list-style-type: circle;">header text color </li>
            <li style="margin:0 0 0 1em;list-style-type: circle;">background color </li>
            <li style="margin:0 0 0 1em;list-style-type: circle; ">link color </li>
            <li style="margin:0 0 0 1em;list-style-type: circle; ">link hover color </li>
            <li style="margin:0 0 0 1em;list-style-type: circle; ">navigation link color </li>
            <li style="margin:0 0 0 1em;list-style-type: circle; ">navigation link hover color </li>
            <li style="margin:0 0 0 1em;list-style-type: circle; ">social media link color </li>
            </ul> 
    </th>
    <th width="50%" align="left" valign="top" scope="col">
         <ul style="margin:0 0 0 2em;">   
            <li style="margin:0 0 0 1em;list-style-type: circle; ">social media link hover color </li>
            <li style="margin:0 0 0 1em;list-style-type: circle; ">headings color </li>
            <li style="margin:0 0 0 1em;list-style-type: circle; ">body text color </li>
            <li style="margin:0 0 0 1em;list-style-type: circle; ">header background color </li>
            <li style="margin:0 0 0 1em;list-style-type: circle; ">footer background color </li>
            <li style="margin:0 0 0 1em;list-style-type: circle; ">footer text color </li>
            <li style="margin:0 0 0 1em;list-style-type: circle; ;">border color</li>
            </ul> 
    </th>
  </tr>
</table> 
 <hr/><br/>
 <p style="margin:1em 0 1em 0;"><span style="font-weight: 900;">PAGE TEMPLATES:</span></p>
 <ul>
 <li>&rarr; Use the <u>Archive Page</u> template to show customized arrays of category defined posts.</li>

 <li>&rarr; <u>Split page</u> and <u>Full page</u> templates are also available.</li>
 </ul>
<hr/><br/>
  <p style="margin:1em 0 1em 0;"><span style="font-weight: 900;">POST TEMPLATES:</span></p>
  <p style="margin:.5em 0 .5em 0;">There is an option to allow for posts to show without a sidebar</p>
  <hr/><br/>

 <p style="margin:.5em 0 .5em 0;"><span style="font-weight: 900;">LINKS AND CONTACT INFORMATION:</span> To set Social Media links, Contact Information and connection to Google Analytics go to <br/>&rarr; appearance <br/>&rarr; Global Custom Fields </p>
<hr/><br/>
 <p style="margin:.5em 0 .5em 0;"><span style="font-weight: 900;">STREAMLINED ADMIN INTERFACE:</span> Clutter has been reduced in the dashboard - in particular the widgets region where the seldom used widgets of dubious importance have been eliminated:</p>
<hr/><br/>
 <p style="margin:.5em 0 .5em 0;"><span style="font-weight: 900;">DOCUMENTATION:</span> For more instructions <a href="https://jeffmcnear.com/wordpress-manuals/plasterdog-flexible.pdf" target="_blank">Download the manual</a></p>
 <p style="margin:0 0 .5em 0;">Still have a question? contact Jeff McNear by email: <a href="mailto:jeff@plasterdog.com?subject=Question about the Plasterdog Customizer theme">here</a>. </p>
<hr/>';
     

     echo '</div>';
}


//https://css-tricks.com/snippets/wordpress/apply-custom-css-to-admin-area/
add_action('admin_head', 'dashboard_cleanup');

function dashboard_cleanup() {
  echo '<style>

    .metabox-holder .postbox-container .empty-container {
    border: none transparent;
    height: 10px!important;
    position: relative;
    min-height:10px!important;
}
@media only screen and (min-width: 900px) {
     #dashboard-widgets .postbox-container:first-of-type {
        width:33% !important;
    }  

   #dashboard-widgets .postbox-container:first-of-type .postbox .inside {
    padding: 0 12px 12px;
    line-height: 1.4em;
    font-size: 13px;
}
 
    #dashboard-widgets .postbox-container {
        width:66%!important;        
    }

    #dashboard-widgets .postbox-container .postbox .inside {
    padding: 0 6em 3em 3em;
}
}
  </style>';
}
