<?php 
/**
 * bio block
 *
 * @package      plasterdogflexible
 * @author       Jeff McNear
 * @since        1.0.0
 * @license      GPL-2.0+
**/

$name = get_field( 'bio_name' );
$title = get_field( 'bio_title' );
$photo = get_field( 'bio_portrait' );
$description = get_field( 'bio_description' );
?>

<div class="bio-container">


<?php if( get_field('block_bio_layout') == 'image_left' ): ?>
<div class="left-image"><img src="<?php echo $photo; ?>"></div><!-- ends left image -->

<div class="right-content">
	<h3><?php echo $name; ?></h3>
	<h4><?php echo $title; ?></h4>
	<?php echo $description; ?>

	<div class="bio-social">
		<?php if ( get_field( 'bio_block_phone' ) ): ?>
		<span class="bio-social-text"><?php the_field('bio_block_phone'); ?></span> 
		<?php else: // field_name returned false ?>
		&nbsp;
		<?php endif; // end of if field_name logic ?>		

			<ul class="bio-social-icons">

				<?php if ( get_field( 'bio_block_facebook' ) ): ?>
				<li><a href="<?php echo get_field('bio_block_facebook') ?>" target="_blank" ><i class="fab fa-facebook-square"></i></a></li>
				<?php endif;  ?>

				<?php if ( get_field( 'bio_block_linkedin' ) ): ?>
				<li><a href="<?php echo get_field('bio_block_linkedin') ?>" target="_blank" ><i class="fab fa-linkedin"></i></a></li>
				<?php endif;  ?>

				<?php if ( get_field( 'bio_block_twitter' ) ): ?>
				<li><a href="<?php echo get_field('bio_block_twitter') ?>" target="_blank" ><i class="fab fa-twitter-square"></i></a></li>
				<?php endif;  ?>

				<?php if ( get_field( 'bio_block_instagram' ) ): ?>
				<li><a href="<?php echo get_field('bio_block_instagram') ?>" target="_blank" ><i class="fab fa-instagram"></i></a></li>
				<?php endif;  ?>	

				<?php if ( get_field( 'bio_block_youtube' ) ): ?>
				<li><a href="<?php echo get_field('bio_block_youtube') ?>" target="_blank" ><i class="fab fa-youtube"></i></a></li>
				<?php endif;  ?>

				<?php if ( get_field( 'bio_block_vimeo' ) ): ?>
				<li><a href="<?php echo get_field('bio_block_vimeo') ?>" target="_blank" ><i class="fab fa-vimeo-square"></i></a></li>
				<?php endif;  ?>

				<?php if ( get_field( 'bio_block_email_address' ) ): ?>
				<li><a href="mailto:<?php echo get_field('bio_block_email_address') ?>?subject=Website Response"><i class="fas fa-envelope"></i></a></li>
				<?php endif;  ?>

			</ul>
	</div><!-- ends bio social -->
</div><!-- ends rigt content -->
<?php endif; ?><!-- the selector clause left -->

<?php if( get_field('block_bio_layout') == 'image_right' ): ?>

<div class="left-image-flipped"><img src="<?php echo $photo; ?>"></div><!-- ends left image -->

<div class="right-content-flipped">
	<h3><?php echo $name; ?></h3>
	<h4><?php echo $title; ?></h4>
	<?php echo $description; ?>

	<div class="bio-social">
		<?php if ( get_field( 'bio_block_phone' ) ): ?>
		<span class="bio-social-text"><?php the_field('bio_block_phone'); ?></span> 
		<?php else: // field_name returned false ?>
		&nbsp;
		<?php endif; // end of if field_name logic ?>		

			<ul class="bio-social-icons">

				<?php if ( get_field( 'bio_block_facebook' ) ): ?>
				<li><a href="<?php echo get_field('bio_block_facebook') ?>" target="_blank" ><i class="fab fa-facebook-square"></i></a></li>
				<?php endif;  ?>

				<?php if ( get_field( 'bio_block_linkedin' ) ): ?>
				<li><a href="<?php echo get_field('bio_block_linkedin') ?>" target="_blank" ><i class="fab fa-linkedin"></i></a></li>
				<?php endif;  ?>

				<?php if ( get_field( 'bio_block_twitter' ) ): ?>
				<li><a href="<?php echo get_field('bio_block_twitter') ?>" target="_blank" ><i class="fab fa-twitter-square"></i></a></li>
				<?php endif;  ?>

				<?php if ( get_field( 'bio_block_instagram' ) ): ?>
				<li><a href="<?php echo get_field('bio_block_instagram') ?>" target="_blank" ><i class="fab fa-instagram"></i></a></li>
				<?php endif;  ?>	

				<?php if ( get_field( 'bio_block_youtube' ) ): ?>
				<li><a href="<?php echo get_field('bio_block_youtube') ?>" target="_blank" ><i class="fab fa-youtube"></i></a></li>
				<?php endif;  ?>

				<?php if ( get_field( 'bio_block_vimeo' ) ): ?>
				<li><a href="<?php echo get_field('bio_block_vimeo') ?>" target="_blank" ><i class="fab fa-vimeo-square"></i></a></li>
				<?php endif;  ?>

				<?php if ( get_field( 'bio_block_email_address' ) ): ?>
				<li><a href="mailto:<?php echo get_field('bio_block_email_address') ?>?subject=Website Response"><i class="fas fa-envelope"></i></a></li>
				<?php endif;  ?>

			</ul>
	</div><!-- ends bio social -->
</div><!-- ends rigt content -->



<?php endif; ?><!-- the selector clause right -->

<div class="clear"></div>
</div><!-- ends bio container -->