	


<div class="cta-container">

<?php if( get_field('block_cta_layout') == 'cta_left' ): ?>

	<div class="cta-feature-container" style="background-color:<?php the_field('block_cta_background_color'); ?>; ">
		<div class="cta-inner-feature">
			<div class="cta-feature-message">
				<a href="<?php the_field('block_cta_link_url'); ?>" style="color:<?php the_field('block_cta_text_color'); ?>;!important; "> <?php the_field('block_cta_link_label'); ?> </a>
			</div><!-- ends feature message-->	
		</div><!-- ends inner feature -->
	</div><!-- ends feature container-->

<?php the_field('block_cta_wrapping_text'); ?>

<?php endif; ?><!-- the selector clause left -->

<?php if( get_field('block_cta_layout') == 'cta_right' ): ?>

	<div class="cta-feature-container-flipped" style="background-color:<?php the_field('block_cta_background_color'); ?>; ">
		<div class="cta-inner-feature-flipped">
			<div class="cta-feature-message">
				<a href="<?php the_field('block_cta_link_url'); ?>" style="color:<?php the_field('block_cta_text_color'); ?>;!important; "> <?php the_field('block_cta_link_label'); ?> </a>
			</div><!-- ends feature message-->	
		</div><!-- ends inner feature -->
	</div><!-- ends feature container-->

<?php the_field('block_cta_wrapping_text'); ?>


<?php endif; ?><!-- the selector clause left -->

</div><!--ends cta-container -->