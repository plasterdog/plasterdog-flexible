
<div class="focus-item-content quotation" style="margin:1em;border-style: solid;background-color:<?php the_field('feature_quote_background_color'); ?>; 
border-width:<?php the_field('feature_quote_border_width'); ?>px; border-color:<?php the_field('feature_quote_border_color'); ?>;  ">

<?php if( get_field('feature_quote_font_style') == 'sans' ): ?>
	<?php if( get_field('feature_quote_font_size') == 'default' ): ?>
		<div class="quote-block-sans" style="color:<?php the_field('feature_quote_text_color'); ?>">		
		<?php the_field('feature_quote_quotation'); ?>
		<p class="attribution" style="color:<?php the_field('feature_quote_text_color'); ?>"><?php the_field('feature_quote_attribution'); ?></p>	
		</div><!--ends quote block -->
	<?php endif; ?><!-- the selector clause default -->

	<?php if( get_field('feature_quote_font_size') == 'bigger' ): ?>
		<div class="quote-block-sans bigger" style="color:<?php the_field('feature_quote_text_color'); ?>">		
		<?php the_field('feature_quote_quotation'); ?>
		<p class="attribution" style="color:<?php the_field('feature_quote_text_color'); ?>"><?php the_field('feature_quote_attribution'); ?></p>	
		</div><!--ends quote block -->
	<?php endif; ?><!-- the selector clause bigger -->
<?php endif; ?><!-- the selector clause sans -->

<?php if( get_field('feature_quote_font_style') == 'serif' ): ?>
	<?php if( get_field('feature_quote_font_size') == 'default' ): ?>
		<div class="quote-block" style="color:<?php the_field('feature_quote_text_color'); ?>">		
		<?php the_field('feature_quote_quotation'); ?>
		<p class="attribution" style="color:<?php the_field('feature_quote_text_color'); ?>"><?php the_field('feature_quote_attribution'); ?></p>	
		</div><!--ends quote block -->
	<?php endif; ?><!-- the selector clause default -->

	<?php if( get_field('feature_quote_font_size') == 'bigger' ): ?>
		<div class="quote-block bigger" style="color:<?php the_field('feature_quote_text_color'); ?>">		
		<?php the_field('feature_quote_quotation'); ?>
		<p class="attribution" style="color:<?php the_field('feature_quote_text_color'); ?>"><?php the_field('feature_quote_attribution'); ?></p>	
		</div><!--ends quote block -->
	<?php endif; ?><!-- the selector clause bigger -->
<?php endif; ?><!-- the selector clause serif -->

</div><!-- ends focus item content -->


