<!-- https://www.w3schools.com/howto/howto_js_accordion.asp -->
<button class="accordion" style="background-color:<?php the_field('block_faq_background_color'); ?>; color:<?php the_field('block_faq_text_color'); ?>;;">
	<?php the_field('block_faq_accordion_teaser');?></button>
<div class="panel fade-in quarter">
 <?php the_field('block_faq_accordion_content');?>
</div>