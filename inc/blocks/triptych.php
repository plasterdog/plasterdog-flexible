		<div class="triptych-section"><!--THE PRIMARY THREE ITEMS-->
				<ul class="three-focus-items">
					<li>
					<div class="inner-focus-item" style="background-color:<?php the_field('block_first_background_color'); ?>">	
						<div class="focus-item-content">

						<?php if ( get_field( 'block_first_feature_title' ) ): ?>	

							<?php if ( get_field( 'block_first_feature_link' ) ): ?>
							<h3><a href="<?php the_field('block_first_feature_link'); ?>"><?php the_field('block_first_feature_title'); ?></a> </h3>
							<?php else: // field_name returned false ?>	
							<h3><?php the_field('block_first_feature_title'); ?> </h3>
							<?php endif; // end of if field_name logic ?>

						<?php else: // field_name returned false ?>	
						<p>&nbsp;</p>
						<?php endif; // end of if field_name logic ?>
						
						<div class="focus-item-excerpt"><?php the_field('block_first_feature_excerpt'); ?></div><!--ends focus item excerpt -->
						<?php if ( get_field( 'block_first_feature_link_label' ) ): ?>
						<h4 style="background-color:<?php the_field('block_first_background_color'); ?>"><a href="<?php the_field('block_first_feature_link'); ?>"><?php the_field('block_first_feature_link_label'); ?></a></h4>	
						

						<?php endif; ?>	
						</div><!-- ends focus item content -->
					</div>
					</li>
					<li>
					<div class="inner-focus-item" style="background-color:<?php the_field('block_second_background_color'); ?>">	
						<div class="focus-item-content">

						<?php if ( get_field( 'block_second_feature_title' ) ): ?>	
							<?php if ( get_field( 'block_second_feature_link' ) ): ?>
							<h3><a href="<?php the_field('block_second_feature_link'); ?>"><?php the_field('block_second_feature_title'); ?></a> </h3>
							<?php else: // field_name returned false ?>
							<h3><?php the_field('block_second_feature_title'); ?> </h3>
							<?php endif; // end of if field_name logic ?>
						<?php else: // field_name returned false ?>	
						<p>&nbsp;</p>
						<?php endif; // end of if field_name logic ?>

						<div class="focus-item-excerpt"><?php the_field('block_second_feature_excerpt'); ?></div><!--ends focus item excerpt -->
						<?php if ( get_field( 'block_second_feature_link_label' ) ): ?>
						<h4 style="background-color:<?php the_field('block_second_background_color'); ?>"><a href="<?php the_field('block_second_feature_link'); ?>"><?php the_field('block_second_feature_link_label'); ?></a></h4>	
						

						<?php endif; ?>	
						</div><!-- ends focus item content -->
					</div>
					</li>
					<li>		
					<div class="inner-focus-item" style="background-color:<?php the_field('block_third_background_color'); ?>">
						<div class="focus-item-content">

						<?php if ( get_field( 'block_third_feature_title' ) ): ?>	
							<?php if ( get_field( 'block_third_feature_link' ) ): ?>	
							<h3><a href="<?php the_field('block_third_feature_link'); ?>"><?php the_field('block_third_feature_title'); ?> </a></h3>
							<?php else: // field_name returned false ?>
							<h3><?php the_field('block_third_feature_title'); ?> </h3>
							<?php endif; // end of if field_name logic ?>
						<?php else: // field_name returned false ?>	
						<p>&nbsp;</p>
						<?php endif; // end of if field_name logic ?>

						<div class="focus-item-excerpt"><?php the_field('block_third_feature_excerpt'); ?></div><!--ends focus item excerpt -->
						<?php if ( get_field( 'block_third_feature_link_label' ) ): ?>
						<h4 style="background-color:<?php the_field('block_third_background_color'); ?>"><a href="<?php the_field('block_third_feature_link'); ?>"><?php the_field('block_third_feature_link_label'); ?></a></h4>
						

						<?php endif; ?>
						</div><!-- ends focus item content -->
					</div>	
					</li>			
				</ul>
		</div><!-- ends section -->