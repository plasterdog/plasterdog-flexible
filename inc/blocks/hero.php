<?php if ( get_field( 'hero_block_link_target' ) ): ?>

<div class="hero-image clear"> <a href="<?php the_field('hero_block_link_target'); ?>"><img src="<?php the_field('hero_block_image'); ?>"/></a>
	<div class="hero-caption-link">
		<h4><a href="<?php the_field('hero_block_link_target'); ?>"><?php the_field('hero_block_call_to_action'); ?></a></h4>
	</div><!-- ends hero caption -->
</div>	<!-- ends hero image -->

<?php else: // field_name returned false ?>	

<div class="hero-image clear"><img src="<?php the_field('hero_block_image'); ?>"/>
	<div class="hero-caption-link">
		<h4><?php the_field('hero_block_call_to_action'); ?></h4>
	</div><!-- ends hero caption -->
</div>	<!-- ends hero image -->

<?php endif; // end of if field_name logic ?>


