<?php
/**
 * @package plasterdogflexible
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">

		

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-summary">

		<div class="archive_left_picture">
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'medium' ); ?></a>
		</div><!-- ends archive left picture -->

		<div class="archive_right_text">
			<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
			<?php the_excerpt(); ?>
	    <p align="right"><a href="<?php the_permalink(); ?>" rel="bookmark">... read the rest...</a></p>
	    </div><!-- ends archive right text-->

	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">

		<div class="archive_left_picture">
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'medium' ); ?></a>
		</div><!-- ends archive left picture -->

		<div class="archive_right_text">
			<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
			<?php the_excerpt(); ?>
	    <p align="right"><a href="<?php the_permalink(); ?>" rel="bookmark">... read the rest...</a></p>
	    </div><!-- ends archive right text-->

	</div><!-- .entry-content -->
</article><!-- #post-## -->
<div class="clear"><hr/></div>

	<?php endif; ?>

	<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
	
	<?php endif; ?>

		
	


