<?php
/**
 * Template Name: Full Width Post
 * Template Post Type: post
 *
 * @package plasterdogcustomizer
 */

get_header(); ?>

<div class="big-background">

    <div id="page" class="hfeed site">
  <div id="content" class="site-content" >
  <div id="primary" class="full-content-area">
    <main id="main" class="full-site-main" role="main">

    <?php while ( have_posts() ) : the_post(); ?>

  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <header class="entry-header">
  <div class="entry-meta"></div><!-- .entry-meta -->
  </header><!-- .entry-header -->
  <div class="entry-content">
    <h1><?php the_title(); ?></h1>
<?php if(get_field('post_author_name')) {?>
  <small>written by: <?php the_field('post_author_name'); ?></small>
  <hr/>
<?php } ?><!-- ends the first condition -->
<?php if(!get_field('post_author_name')) {?>
    
<?php }?> <!-- ends the second outer condition -->  
    <?php the_content(); ?>

<?php the_tags( __( 'Tags: ', 'plasterdogcustomizer' ), ' ', '' ); ?>
  </div><!-- .entry-content -->

  <footer class="entry-footer">
    

    <?php edit_post_link( __( 'Edit', 'plasterdogcustomizer' ), '<span class="edit-link">', '</span>' ); ?>
  </footer><!-- .entry-footer -->
</article><!-- #post-## -->

        <div class="clear"><!-- variation from default nav which restricts navigation within category -->
<div class="left-split-nav"><?php //previous_post_link('%link', '&larr; %title', TRUE) ?></div>
<div class="right-split-nav"><?php //next_post_link('%link', '%title &rarr;', TRUE) ?></div>
</div>

      <?php
        // If comments are open or we have at least one comment, load up the comment template
        if ( comments_open() || '0' != get_comments_number() ) :
          comments_template();
        endif;
      ?>

    <?php endwhile; // end of the loop. ?>

    </main><!-- #main -->
  </div><!-- #primary -->

  <div class="clear" style="height:2em;"></div>
</div><!-- ENDS BIG BACKGROUND -->
<?php get_footer(); ?>